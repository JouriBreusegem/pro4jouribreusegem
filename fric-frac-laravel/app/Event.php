<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
      'name',
      'location',
      'starts',
      'ends',
      'image',
      'description',
      'organiserdescription',
      'category_id',
      'topic_id',
      'person_id',
    ];
}
