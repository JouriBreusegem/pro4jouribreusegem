<?php

namespace App\Http\Controllers;

use App\Person;
use App\Role;
use App\User;
use App\Userinfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Userinfo::all();
        return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $persons = Person::all();
        $roles = Role::all();
        return view('user.create', compact('persons', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'person_id' => 'required',
            'role_id' => 'required',
            'salt' => 'required',
            'hashedpassword' => 'required',
        ]);

        Userinfo::create($request->all());
        return redirect()->route('user.index')

            ->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Userinfo::find($id);

        return view('user.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Userinfo::find($id);
        $persons = Person::all();
        $roles = Role::all();

        return view('user.edit', compact('item', 'persons', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'person_id' => 'required',
            'role_id' => 'required',
            'salt' => 'required',
            'hashedpassword' => 'required',
        ]);

        $update = [
            'name' => $request->name,
            'person_id' => $request->person_id,
            'role_id' => $request->role_id,
            'salt' => $request->salt,
            'hashedpassword' => $request->hashedpassword,
        ];

        Userinfo::where('id',$id)->update($update);

        return Redirect::to(route('user.index'))
            ->with('success', 'User is aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Userinfo  $userinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Userinfo::where('id',$id)->delete();

        return Redirect::to(route('user.index'))->with('success','User deleted successfully');
    }
}
