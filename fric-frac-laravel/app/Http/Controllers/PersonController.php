<?php

namespace App\Http\Controllers;

use App\Country;
use App\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $person = Person::all();
        return view('person.index', compact('person'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('person.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'country_id' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
            'phone1' => 'required',
            'birthday' => 'required',
            'rating' => 'required',
        ]);

        Person::create($request->all());
        return redirect()->route('person.index')

            ->with('success', 'Person created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Person::find($id);
        return view('person.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Person::find($id);
        $countries = Country::all();
        return view('person.edit', compact('item', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'country_id' => 'required',
            'email' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'postalcode' => 'required',
            'city' => 'required',
            'phone1' => 'required',
            'birthday' => 'required',
            'rating' => 'required',
        ]);

        $update = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'country_id' => $request->country_id,
            'email' => $request->email,
            'address1' => $request->address1,
            'address2' => $request->address2,
            'postalcode' => $request->postalcode,
            'city' => $request->city,
            'phone1' => $request->phone1,
            'birthday' => $request->birthday,
            'rating' => $request->rating
        ];

        Person::where('id',$id)->update($update);

        return Redirect::to(route('person.index'))
            ->with('success', 'Person is aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Person::where('id',$id)->delete();

        return Redirect::to(route('person.index'))->with('success','Person deleted successfully');
    }
}
