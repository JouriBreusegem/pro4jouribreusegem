<?php

namespace App\Http\Controllers;

use App\Event;
use App\Person;
use App\EventCategory;
use App\EventTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();
        return view('event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $persons = Person::all();
        $categories = EventCategory::all();
        $topics = EventTopic::all();

        return view('event.create', compact('persons', 'categories', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'image' => 'required',
            'description' => 'required',
            'organiserdescription' => 'required',
            'category_id' => 'required',
            'topic_id' => 'required',
            'person_id' => 'required',
        ]);

        Event::create($request->all());
        return redirect()->route('event.index')

            ->with('success', 'Event created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Event::find($id);

        return view('event.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Event::find($id);
        $persons = Person::all();
        $categories = EventCategory::all();
        $topics = EventTopic::all();

        return view('event.edit', compact('item', 'persons', 'categories', 'topics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'location' => 'required',
            'starts' => 'required',
            'ends' => 'required',
            'image' => 'required',
            'description' => 'required',
            'organiserdescription' => 'required',
            'category_id' => 'required',
            'topic_id' => 'required',
            'person_id' => 'required',
        ]);

        $update = [
            'name' => $request->name,
            'location' => $request->location,
            'starts' => $request->starts,
            'ends' => $request->ends,
            'image' => $request->image,
            'description' => $request->description,
            'organiserdescription' => $request->organiserdescription,
            'category_id' => $request->category_id,
            'topic_id' => $request->topic_id,
            'person_id' => $request->person_id,
        ];

        Event::where('id',$id)->update($update);

        return Redirect::to(route('event.index'))
            ->with('success', 'Event is aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::where('id',$id)->delete();

        return Redirect::to(route('event.index'))->with('success','Event deleted successfully');
    }
}
