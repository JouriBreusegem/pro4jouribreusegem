<?php
/*
https://www.positronx.io/php-laravel-crud-operations-mysql-tutorial/
https://www.itsolutionstuff.com/post/laravel-7-crud-example-laravel-7-tutorial-for-beginnersexample.html
*/
namespace App\Http\Controllers;

use App\EventTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventTopic = EventTopic::all();
        return view('eventtopic.index', compact('eventTopic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventtopic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventTopic::create($request->all());
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventCategory  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = EventTopic::find($id);
        return view('eventtopic.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCategory  $eventTOpic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = EventTopic::find($id);
        return view('eventtopic.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $update = ['name' => $request->name];
        EventTopic::where('id',$id)->update($update);

        return Redirect::to(route('eventtopic.index'))
            ->with('success', 'Topic is aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventTopic  $eventTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EventTopic::where('id',$id)->delete();

        return Redirect::to(route('eventtopic.index'))->with('success','Topic deleted successfully');
    }
}
