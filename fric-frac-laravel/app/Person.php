<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'firstname',
        'country_id',
        'lastname',
        'email',
        'address1',
        'address2',
        'postalcode',
        'city',
        'phone1',
        'birthday',
        'rating',
    ];
}
