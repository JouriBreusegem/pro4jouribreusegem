<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected  $fillable = [
        'name',
        'person_id',
        'role_id',
        'salt',
        'hashedpassword'
    ];
}
