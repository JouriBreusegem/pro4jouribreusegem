@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Person</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('person.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('person.update', $item->id) }}" method="POST">
        @method('PATCH')
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>FirstName:</strong>
                    <input type="text" name="firstname" class="form-control" value="{{ $item->firstname }}">
                    <p></p>
                    <strong>LastName:</strong>
                    <input type="text" name="lastname" class="form-control" value="{{ $item->lastname }}">
                    <p></p>
                    <strong>Email:</strong>
                    <input type="text" name="email" class="form-control" value="{{ $item->email }}">
                    <p></p>
                    <strong>Address1:</strong>
                    <input type="text" name="address1" class="form-control" value="{{ $item->address1 }}">
                    <p></p>
                    <strong>Address2:</strong>
                    <input type="text" name="address2" class="form-control" value="{{ $item->address2 }}">
                    <p></p>
                    <strong>PostalCode:</strong>
                    <input type="text" name="postalcode" class="form-control" value="{{ $item->postalcode }}">
                    <p></p>
                    <strong>City:</strong>
                    <input type="text" name="city" class="form-control" value="{{ $item->city }}">
                    <p></p>
                    <strong>Country:</strong><br>
                    <select name="country_id" class="form-control">
                        @foreach($countries as $country)
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Phone1:</strong>
                    <input type="text" name="phone1" class="form-control" value="{{ $item->phone1 }}">
                    <p></p>
                    <strong>birthday:</strong>
                    <input type="date" name="birthday" class="form-control" value="{{ $item->birthday }}">
                    <p></p>
                    <strong>Rating:</strong>
                    <input type="number" name="rating" class="form-control" value="{{ $item->rating }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
