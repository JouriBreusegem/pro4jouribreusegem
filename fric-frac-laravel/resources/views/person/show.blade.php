@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('person.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <a class="btn btn-info" href="{{ route('person.edit', $item->id) }}"> Edit</a>
    <form action="{{ route('person.destroy', $item->id)}}" method="POST" style="display:inline-block">
        @csrf
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger" type="submit"> Delete</button>
    </form>
    <p></p>
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <td>{{ $item->id }}</td>

        </tr>
        <tr>
            <th>FirstName</th>
            <td>{{ $item->firstname }}</td>
        </tr>
        <tr>
            <th>LastName</th>
            <td>{{ $item->lastname }}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $item->email }}</td>
        </tr>
        <tr>
            <th>Address1</th>
            <td>{{ $item->address1 }}</td>
        </tr>
        <tr>
            <th>Address2</th>
            <td>{{ $item->address2 }}</td>
        </tr>
        <tr>
            <th>PostalCode</th>
            <td>{{ $item->postalcode }}</td>
        </tr>
        <tr>
            <th>City</th>
            <td>{{ $item->city }}</td>
        </tr>
        <tr>
            <th>Country ID</th>
            <td>{{ $item->country_id }}</td>
        </tr>
        <tr>
            <th>Phone1</th>
            <td>{{ $item->phone1 }}</td>
        </tr>
        <tr>
            <th>Birthday</th>
            <td>{{ $item->birthday }}</td>
        </tr>
        <tr>
            <th>Rating</th>
            <td>{{ $item->rating }}</td>
        </tr>
    </table>
@endsection
