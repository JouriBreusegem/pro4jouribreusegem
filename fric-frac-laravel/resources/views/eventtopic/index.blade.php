@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <a type="button" href="/" class="btn btn-primary">Home</a>
                <a class="btn btn-success" href="{{ route('eventtopic.create') }}"> Create New EventCategory</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($eventTopic as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>
                    <a class="btn btn-success" href="{{ route('eventtopic.show', $item->id) }}" method="POST"> Show</a>
                    <a class="btn btn-info" href="{{ route('eventtopic.edit', $item->id) }}"> Edit</a>
                    <form action="{{ route('eventtopic.destroy', $item->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit"> Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
