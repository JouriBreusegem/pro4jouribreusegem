@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <a class="btn btn-info" href="{{ route('user.edit', $item->id) }}"> Edit</a>
    <form action="{{ route('user.destroy', $item->id)}}" method="POST" style="display:inline-block">
        @csrf
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger" type="submit"> Delete</button>
    </form>
    <p></p>
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <td>{{ $item->id }}</td>

        </tr>
        <tr>
            <th>Name</th>
            <td>{{ $item->name }}</td>
        </tr>
        <tr>
            <th>Person ID</th>
            <td>{{ $item->person_id }}</td>
        </tr>
        <tr>
            <th>Role ID</th>
            <td>{{ $item->role_id }}</td>
        </tr>
        <tr>
            <th>Salt</th>
            <td>{{ $item->salt }}</td>
        </tr>
        <tr>
            <th>HashedPassword</th>
            <td>{{ $item->hashedpassword }}</td>
        </tr>
    </table>
@endsection
