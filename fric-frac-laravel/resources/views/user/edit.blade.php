@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('user.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('user.update', $item->id) }}" method="POST">
        @method('PATCH')
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" value="{{ $item->name }}">
                    <p></p>
                    <strong>Person ID:</strong>
                    <select name="person_id" class="form-control">
                        @foreach($persons as $person)
                            <option value="{{ $person->id }}">{{ $person->firstname }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Role ID:</strong>
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Salt:</strong>
                    <input type="text" name="salt" class="form-control" value="{{ $item->salt }}">
                    <p></p>
                    <strong>HashedPassword:</strong>
                    <input type="text" name="hashedpassword" class="form-control" value="{{ $item->hashedpassword }}">
                    <p></p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
