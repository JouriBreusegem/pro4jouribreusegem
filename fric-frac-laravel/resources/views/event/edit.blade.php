@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Event</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('event.update', $item->id) }}" method="POST">
        @method('PATCH')
        @csrf
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" value="{{ $item->name }}">
                    <p></p>
                    <strong>Person:</strong>
                    <select name="person_id" class="form-control">
                        @foreach($persons as $person)
                            <option value="{{ $person->id }}">{{ $person->firstname }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Category:</strong>
                    <select name="category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Topic:</strong>
                    <select name="topic_id" class="form-control">
                        @foreach($topics as $topic)
                            <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                        @endforeach
                    </select>
                    <p></p>
                    <strong>Location:</strong>
                    <input type="text" name="location" class="form-control" value="{{ $item->location }}">
                    <p></p>
                    <strong>Starts:</strong>
                    <input type="date" name="starts" class="form-control" value="{{ $item->starts }}">
                    <p></p>
                    <strong>Ends:</strong>
                    <input type="date" name="ends" class="form-control" value="{{ $item->ends }}">
                    <p></p>
                    <strong>Image:</strong>
                    <input type="text" name="image" class="form-control" value="{{ $item->image }}">
                    <p></p>
                    <strong>Description:</strong>
                    <input type="text" name="description" class="form-control" value="{{ $item->description }}">
                    <p></p>
                    <strong>Organiser Description:</strong>
                    <input type="text" name="organiserdescription" class="form-control" value="{{ $item->organiserdescription }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
