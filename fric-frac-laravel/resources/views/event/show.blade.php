@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <a class="btn btn-info" href="{{ route('event.edit', $item->id) }}"> Edit</a>
    <form action="{{ route('event.destroy', $item->id)}}" method="POST" style="display:inline-block">
        @csrf
        <input name="_method" type="hidden" value="DELETE">
        <button class="btn btn-danger" type="submit"> Delete</button>
    </form>
    <p></p>
    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <td>{{ $item->id }}</td>

        </tr>
        <tr>
            <th>Name</th>
            <td>{{ $item->name }}</td>
        </tr>
        <tr>
            <th>Person ID</th>
            <td>{{ $item->person_id }}</td>
        </tr>
        <tr>
            <th>Category ID</th>
            <td>{{ $item->category_id }}</td>
        </tr>
        <tr>
            <th>Topic ID</th>
            <td>{{ $item->topic_id }}</td>
        </tr>
        <tr>
            <th>Location</th>
            <td>{{ $item->location }}</td>
        </tr>
        <tr>
            <th>Starts</th>
            <td>{{ $item->starts }}</td>
        </tr>
        <tr>
            <th>Ends</th>
            <td>{{ $item->ends }}</td>
        </tr>
        <tr>
            <th>Image</th>
            <td>{{ $item->image }}</td>
        </tr>
        <tr>
            <th>Description</th>
            <td>{{ $item->description }}</td>
        </tr>
        <tr>
            <th>Organiser Description</th>
            <td>{{ $item->organiserdescription }}</td>
        </tr>
    </table>
@endsection
