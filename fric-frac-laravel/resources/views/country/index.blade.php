@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 7 CRUD Fric-frac</h2>
            </div>
            <div class="pull-right">
                <a type="button" href="/" class="btn btn-primary">Home</a>
                <a class="btn btn-success" href="{{ route('country.create') }}"> Create New Country</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Code</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($country as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->code }}</td>
                <td>
                    <a class="btn btn-success" href="{{ route('country.show', $item->id) }}" method="POST"> Show</a>
                    <a class="btn btn-info" href="{{ route('country.edit', $item->id) }}"> Edit</a>
                    <form action="{{ route('country.destroy', $item->id)}}" method="POST" style="display:inline-block">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit"> Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
