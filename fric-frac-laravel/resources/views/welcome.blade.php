<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    </head>
    <body>
        <div class="text-center" style="padding-top:250px">
            <a type="button" href="{{ route('eventtopic.index') }}" class="btn btn-primary btn-lg align-middle">EventTopic</a>
            <a type="button" href="{{ route('eventcategory.index') }}" class="btn btn-primary btn-lg">EventCategory</a>
            <a type="button" href="{{ route('event.index') }}" class="btn btn-primary btn-lg">Event</a>
            <a type="button" href="{{ route('country.index') }}" class="btn btn-primary btn-lg">Country</a>
            <a type="button" href="{{ route('person.index') }}" class="btn btn-primary btn-lg">Person</a>
            <a type="button" href="{{ route('user.index') }}" class="btn btn-primary btn-lg">User</a>
            <a type="button" href="{{ route('role.index') }}" class="btn btn-primary btn-lg">Role</a>
        </div>

    </body>
</html>
