SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SET GLOBAL sql_mode = 'ANSI';

-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS fricfraclaravel;

USE fricfraclaravel;

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE EventCategory
-- Created on Monday 4th of September 2017 02:57:37 PM
--
DROP TABLE IF EXISTS `event_categories`;
CREATE TABLE `event_categories`(
    `Name` NVARCHAR (120) NOT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id),
    CONSTRAINT uc_EventCategory_Name UNIQUE (Name));
    
-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE EventTopic
-- Created on Tuesday 17th of March 2020 01:43:17 PM
-- 
DROP TABLE IF EXISTS `event_topics`;
CREATE TABLE `event_topics`(
	`Name` NVARCHAR (120) NOT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT uc_EventTopic_Name UNIQUE (Name));
    
-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Event
-- Created on Tuesday 17th of March 2020 01:43:17 PM
-- 
DROP TABLE IF EXISTS `Event`;
CREATE TABLE `Event`(
	`Name` NVARCHAR (120) NOT NULL,
	`Location` NVARCHAR (120) NOT NULL,
	`Starts` DATE NULL,
	`Ends` DATE NULL,
	`Image` NVARCHAR (255) NOT NULL,
	`Description` NVARCHAR (1024) NOT NULL,
	`OrganiserDescription` NVARCHAR (120) NOT NULL,
	`EventCategoryId` INT NULL,
	`EventTopicId` INT NULL,
    `PersonId` INT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT fk_EventEventCategoryId FOREIGN KEY (`EventCategoryId`) REFERENCES `EventCategory` (`Id`),
	CONSTRAINT fk_EventPersonId FOREIGN KEY (`PersonId`) REFERENCES `Person` (`Id`),
	CONSTRAINT fk_EventEventTopicId FOREIGN KEY (`EventTopicId`) REFERENCES `EventTopic` (`Id`));
    
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
	`Name` NVARCHAR (120) NOT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id));
    
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User`(
	`Name` NVARCHAR (120) NOT NULL,
    `Salt` NVARCHAR (120) NOT NULL,
    `HashedPassword` NVARCHAR(120) NOT NULL,
    `RoleId` INT NULL,
    `PersonId` INT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id),
    CONSTRAINT fk_UserRoleId FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`),
    CONSTRAINT fk_UserPersonId FOREIGN KEY (`PersonId`) REFERENCES `Person` (`Id`));
    
DROP TABLE IF EXISTS `Person`;
CREATE TABLE `Person`(
	`FirstName` NVARCHAR (120) NOT NULL,
    `LastName` NVARCHAR (120) NOT NULL,
    `Email` NVARCHAR(120) NOT NULL,
	`Address1` NVARCHAR(250) NOT NULL,
    `Address2` NVARCHAR(250) NOT NULL,
    `PostalCode` NVARCHAR(80) NOT NULL,
    `City` NVARCHAR(120) NOT NULL,
    `Phone1` NVARCHAR(120) NOT NULL,
    `Birthday` DATE NOT NULL,
    `Rating` INT NOT NULL,
    `CountryId` INT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id),
    CONSTRAINT fk_PersonCountryId FOREIGN KEY (`CountryId`) REFERENCES `Country` (`Id`));

DROP TABLE IF EXISTS `Country`;
CREATE TABLE `Country`(
	`Name` NVARCHAR (120) NOT NULL,
    `Code` NVARCHAR (120) NOT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id));
    
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;