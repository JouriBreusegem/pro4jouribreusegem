<?php
if (isset($_POST['submit'])) {
    echo 'hallo ik ben PHP!!!!';
    // var_dump($_POST);
    $uFirstName = strtoupper($_POST['firstName']);
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["firstname"])) {
            $FirstNameErr = "Voornaam is verplicht";
        } else {
            $uFirstName = strtoupper($_POST['firstname']);
        }

        if (empty($_POST["lastName"])) {
            $lastNameErr = "Familienaam is verplicht";
        } else {
            $uLastName = strtoupper($_POST['lastName']);
        }

        if (empty($_POST["email"])) {
            $emailErr = "Email is verplicht";
        } else {
            $uEmailErr = strtoupper($_POST['email']);
        }

        if (empty($_POST["password1"])) {
            $lastNameErr = "Password1 is verplicht";
        } else {
            $uLastName = strtoupper($_POST['password1']);
        }

        if (empty($_POST["password2"])) {
            $lastNameErr = "Password2 is verplicht";
        } else {
            $uLastName = strtoupper($_POST['password2']);
        }

        if (empty($_POST["address1"])) {
            $lastNameErr = "Address1 is verplicht";
        } else {
            $uLastName = strtoupper($_POST['address1']);
        }
    }
}
?>

<html>
	<head>
		<title>PHP form validatie</title>
	</head>
<body>
	<form>
		<fieldset>
			<legend>Accountgegevens:</legend>
			Voornaam:	<span>*</span> &nbsp <input id="firstname" name="firstname" type="text" required><br>
			Familienaam:	<span>*</span> &nbsp <input id="lastname" name="lastname" type="text" required><br>
			E-mail:		<span>*</span> &nbsp <input id="email" name="email" type="text" placeholder="name@example.com" required><br>
			Wachtwoord:		<span>*</span> &nbsp <input id="password1" name="password1" type="password" required><br>
			Wachtwoord bevestigen:		<span>*</span> &nbsp <input id="password2" name="password2" type="password" required>
		</fieldset>
		<fieldset>
			<legend>Adresgegevens</legend>
			Adres 1:	<span>*</span> &nbsp <input id="address1" name="address1" type="text" required><br>
			Adres 2:	<input id="address2" name="address2" type="text"><br>
			Stad:	<span>*</span> &nbsp <input id="city" name="city" type="text" required><br>
			Postcode:	<span>*</span> &nbsp <input id="postalcode" name="postalcode" type="number" required><br>
		</fieldset>
		<fieldset>
			<legend>Persoonlijke gegevens</legend>
			Geboortedatum:	<input id="birthday" name="birthday" type="date"><br>
			Hoe tevreden ben je?	<input id="statisfied" name="statisfied" type="range" min="0" max="10"><br>
			<input id="male" name="sex" type="radio">Man</input>
			<input id="female" name="sex" type="radio">Vrouw</input>
			<input id="unknown" name="sex" type="radio">Onbekend</input><br>
			Kies een module:	<select id="course" name="course" name="modules">
									<option value="module1">Module 1</option>
									<option value="module2">Module 2</option>
									<option value="module3">Module 3</option>
									<option value="module4">Module 4</option>
									<option value="module5">Module 5</option>
									<option value="module6">Module 6</option>
								</select>
		</fieldset>
        <button name="submit" type="submit" value="submit">
            Verzenden
        </button>
        <input type="submit" value="submit" name="submit" />
	</form>
</body>
</html>