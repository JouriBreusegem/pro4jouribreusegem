<?php
if (isset($_POST['submit'])) {
    require "../../common.php";
    require "../../config.php";

    try {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_topic = array(
            "Name" => $_POST['name']
        );

        $sql = sprintf("INSERT INTO EventTopic(Name) VALUES ('%s')", $_POST['name']);

        $statement = $connection->prepare($sql);
        $statement->execute($new_topic);
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>

<?php include "../templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) { ?>
    <?php echo escape($_POST['name']); ?> successfully added.
<?php } ?>

<h2>Add Topic</h2>

<form method="post">
    <label for="name">Name</label>
    <input type="text" name="name" id="name">
    <br>
    <input type="submit" name="submit" value="Submit">
</form>

<a href="../index.php">Back to home</a>

<?php include "../templates/footer.php"; ?>
