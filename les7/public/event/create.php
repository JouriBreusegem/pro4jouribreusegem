<?php
require "../../common.php";
require "../../config.php";

if (isset($_POST['submit'])) {
    try {
        $connection = new PDO($dsn, $username, $password, $options);

        $new_event = array(
            "Name" => $_POST['name'],
            "Location" => $_POST['location'],
            "Starts" => $_POST['starts'],
            "Ends" => $_POST['ends'],
            "Image" => $_POST['image'],
            "Description" => $_POST['description'],
            "OrganiserName" => $_POST['organisername'],
            "OrganiserDescription" => $_POST['organiserdescription'],
            "EventTopicId" => $_POST['eventtopicid'],
            "EventCategoryId" => $_POST['eventcategoryid'],
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            "Event",
            implode(", ", array_keys($new_event)),
            ": " . implode(", :", array_keys($new_event))
        );

        $statement = $connection->prepare($sql);
        $statement->execute($new_event);
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
try {
    $connection = new PDO($dsn, $username, $password, $options);

    $sql = "SELECT * FROM EventCategory";

    $statement = $connection->prepare($sql);
    $statement->execute();

   $categoryList = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
try {
    $connection = new PDO($dsn, $username, $password, $options);

    $sql = "SELECT * FROM EventTopic";

    $statement = $connection->prepare($sql);
    $statement->execute();

    $topicList = $statement->fetchAll();
} catch(PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
?>

<?php include "../templates/header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) { ?>
    <?php echo escape($_POST['name']); ?> successfully added.
<?php } ?>

<h2>Add Event</h2>

<form method="post">
    <label for="name">Name</label>
    <input type="text" name="name" id="name"><br>
    <label for="location">Location</label>
    <input type="text" name="location" id="location"><br>
    <label for="starts">Starts</label>
    <input type="date" name="starts" id="starts"><br>
    <label for="ends">Ends</label>
    <input type="date" name="ends" id="ends"><br>
    <label for="image">Image</label>
    <input type="text" name="image" id="image"><br>
    <label for="description">Description</label>
    <input type="text" name="description" id="description"><br>
    <label for="organisername">Organiser Name</label>
    <input type="text" name="organisername" id="organisername"><br>
    <label for="organiserdescription">Organiser Description</label>
    <input type="text" name="organiserdescription" id="organiserdescription"><br>
    <label for="eventtopicid">Event topic</label>
    <select id="eventtopicid" name="eventtopicid" readonly>
        <!-- option elementen -->
        <?php
        if ($topicList) {
            foreach ($topicList as $row) {
                ?>
                <option value="<?php echo $row['id'];?>">
                    <?php echo $row['Name'];?>
                </option>
                <?php
            }
        }
        ?>
    </select><br>
    <label for="eventcategoryid">Event category</label>
    <select id="eventcategoryid" name="eventcategoryid" readonly>
        <!-- option elementen -->
        <?php
        if ($categoryList) {
            foreach ($categoryList as $row) {
                ?>
                <option value="<?php echo $row['id'];?>">
                    <?php echo $row['Name'];?>
                </option>
                <?php
            }
        }
        ?>
    </select><br>
    <input type="submit" name="submit" value="Submit">
</form>

<a href="../index.php">Back to home</a>

<?php include "../templates/footer.php"; ?>
