<?php include "templates/header.php"; ?>
    <ol>
        <li>Event Topic
            <ul>
                <li><a href="eventTopic/create.php"><strong>Create</strong></a> voeg een topic toe</li>
                <li><a href="eventTopic/read.php"><strong>Read</strong></a> - zoek een topic</li>
                <li><a href="eventTopic/update.php"><strong>Update</strong></a> - wijzig een topic</li>
                <li><a href="eventTopic/delete.php"><strong>Delete</strong></a> - delete een topic</li>
            </ul>
        </li>
        <br>
        <li>Event Category
            <ul>
                <li><a href="eventCategory/create.php"><strong>Create</strong></a> voeg een categorie toe</li>
                <li><a href="eventCategory/read.php"><strong>Read</strong></a> - zoek een categorie</li>
                <li><a href="eventCategory/update.php"><strong>Update</strong></a> - wijzig een categorie</li>
                <li><a href="eventCategory/delete.php"><strong>Delete</strong></a> - delete een categorie</li>
            </ul>
        </li>
        <br>
        <li>Event
            <ul>
                <li><a href="event/create.php"><strong>Create</strong></a> voeg een event toe</li>
                <li><a href="event/read.php"><strong>Read</strong></a> - zoek een event</li>
                <li><a href="event/update.php"><strong>Update</strong></a> - wijzig een event</li>
                <li><a href="event/delete.php"><strong>Delete</strong></a> - delete een event</li>
            </ul>
        </li>
    </ol>

<?php include "templates/footer.php"; ?>