USE fricfracjouri;

INSERT INTO EventTopic (Name)
VALUES
    ('Auto, Boat & Air'),
    ('Business & Professional'),
    ('Charities & Causes'),
    ('Community & Culture'),
    ('Family & Education'),
    ('Fashion & Beauty'),
    ('Film. Media & Entertainment'),
    ('Food & Drink'),
    ('Government & Politics'),
    ('Health & Wellness'),
    ('Hobbies & Special Interests'),
    ('Home & Lifestyle'),
    ('Music'),
    ('Other'),
    ('Performing& Visual Arts'),
    ('Religion & Spirituality'),
    ('Science & Technology'),
    ('Seasonal'),
    ('Sports & Fitness'),
    ('Travel & Outdoor');