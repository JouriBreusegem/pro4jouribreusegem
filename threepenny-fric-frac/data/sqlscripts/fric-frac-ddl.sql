SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SET GLOBAL sql_mode = 'ANSI';

-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS fricfracjouri;

USE fricfracjouri;

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE EventCategory
-- Created on Monday 4th of September 2017 02:57:37 PM
--
DROP TABLE IF EXISTS `EventCategory`;
CREATE TABLE `EventCategory` (
    `Name` NVARCHAR (120) NOT NULL,
    `Id` INT NOT NULL AUTO_INCREMENT,
    CONSTRAINT PRIMARY KEY(Id),
    CONSTRAINT uc_EventCategory_Name UNIQUE (Name));
    
-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE EventTopic
-- Created on Tuesday 17th of March 2020 01:43:17 PM
-- 
DROP TABLE IF EXISTS `EventTopic`;
CREATE TABLE `EventTopic` (
	`Name` NVARCHAR (120) NOT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT uc_EventTopic_Name UNIQUE (Name));
    
-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Event
-- Created on Tuesday 17th of March 2020 01:43:17 PM
-- 
DROP TABLE IF EXISTS `Event`;
CREATE TABLE `Event` (
	`Name` NVARCHAR (120) NOT NULL,
	`Location` NVARCHAR (120) NOT NULL,
	`Starts` DATETIME NULL,
	`Ends` DATETIME NULL,
	`Image` NVARCHAR (255) NOT NULL,
	`Description` NVARCHAR (1024) NOT NULL,
	`OrganiserName` NVARCHAR (120) NOT NULL,
	`OrganiserDescription` NVARCHAR (120) NOT NULL,
	`EventCategoryId` INT NULL,
	`EventTopicId` INT NULL,
	`Id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT PRIMARY KEY(Id),
	CONSTRAINT fk_EventEventCategoryId FOREIGN KEY (`EventCategoryId`) REFERENCES `EventCategory` (`Id`),
	CONSTRAINT fk_EventEventTopicId FOREIGN KEY (`EventTopicId`) REFERENCES `EventTopic` (`Id`));
    
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;