<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class EventTopicController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventTopic = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventTopic', $eventTopic, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$eventTopic['Name']} is toegevoegd aan EventTopic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$eventTopic['Name']} niet toevoegen aan EventTopic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('EventTopic', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
            return $this->view($model, 'Views/EventTopic/Index.php');
        } else {
            return $this->view($model, 'Views/EventTopic/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/EventTopic/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $eventCategory = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name']
        );

        $isUpdated = \AnOrmApart\Dal::update('EventTopic', $eventCategory, 'Name');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
            return $this->view($model, 'Views/EventTopic/Index.php');
        } else {
            return  $this->view($model, 'Views/EventTopic/UpdatingOne.php');
        }
    }
}
