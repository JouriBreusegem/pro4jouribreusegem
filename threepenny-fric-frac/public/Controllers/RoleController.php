<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class RoleController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Role');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Role');
        $model['Message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Role',
            'error' => 'Geen'
        );
        $role = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('Role', $role, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$role['Name']} is toegevoegd aan Role";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$role['Name']} niet toevoegen aan Role";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Role');
        return $this->view($model, 'Views/Role/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Role', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Role');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('Role', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('Role');
            return $this->view($model, 'Views/Role/Index.php');
        } else {
            return $this->view($model, 'Views/Role/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Role', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Role');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/Role/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'Role',
            'error' => 'Geen'
        );
        $role = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name']
        );

        $isUpdated = \AnOrmApart\Dal::update('Role', $role, 'Name');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('Role');
            return $this->view($model, 'Views/Role/Index.php');
        } else {
            return  $this->view($model, 'Views/Role/UpdatingOne.php');
        }
    }
}