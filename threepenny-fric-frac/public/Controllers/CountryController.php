<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class CountryController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Country');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Country');
        $model['Message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Country',
            'error' => 'Geen'
        );
        $country = array(
            "Name" => $_POST['Name'],
            "Code" => $_POST['Code']
        );

        if (\AnOrmApart\Dal::create('Country', $country, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$country['Name']} is toegevoegd aan Country";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$country['Name']} niet toevoegen aan Country";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Country');
        return $this->view($model, 'Views/Country/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Country', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Country');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('Country', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('Country');
            return $this->view($model, 'Views/Country/Index.php');
        } else {
            return $this->view($model, 'Views/Country/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Country', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Country');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/Country/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'Country',
            'error' => 'Geen'
        );
        $country = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name'],
            "Code" => $_POST['Code']
        );

        $isUpdated = \AnOrmApart\Dal::update('Country', $country, 'Name');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('Country');
            return $this->view($model, 'Views/Country/Index.php');
        } else {
            return  $this->view($model, 'Views/Country/UpdatingOne.php');
        }
    }
}