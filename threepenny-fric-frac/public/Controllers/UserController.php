<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class UserController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('User');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('User');
        $model['listPerson'] = \AnOrmApart\Dal::readAll('Person');
        $model['listRole'] = \AnOrmApart\Dal::readAll('Role');
        $model['Message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'User',
            'error' => 'Geen'
        );
        $user = array(
            "Name" => $_POST['Name'],
            "Salt" => $_POST['Salt'],
            "HashedPassword" => $_POST['HashedPassword'],
            "PersonId" => $_POST['PersonId'],
            "RoleId" => $_POST['RoleId']
        );

        if (\AnOrmApart\Dal::create('User', $user, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$user['Name']} is toegevoegd aan User";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$user['Name']} niet toevoegen aan User";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('User');
        return $this->view($model, 'Views/User/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('User', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('User');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('User', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('User');
            return $this->view($model, 'Views/User/Index.php');
        } else {
            return $this->view($model, 'Views/User/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('User', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('User');
        $model['listRole'] = \AnOrmApart\Dal::readAll('Role');
        $model['listPerson'] = \AnOrmApart\Dal::readAll('Person');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/User/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'User',
            'error' => 'Geen'
        );
        $role = array(
            "Id" => $_POST['Id'],
            "Name" => $_POST['Name'],
            "Salt" => $_POST['Salt'],
            "HashedPassword" => $_POST['HashedPassword'],
            "PersonId" => $_POST['PersonId'],
            "RoleId" => $_POST['RoleId']
        );

        $isUpdated = \AnOrmApart\Dal::update('User', $role, 'Name');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('User');
            return $this->view($model, 'Views/User/Index.php');
        } else {
            return  $this->view($model, 'Views/User/UpdatingOne.php');
        }
    }
}