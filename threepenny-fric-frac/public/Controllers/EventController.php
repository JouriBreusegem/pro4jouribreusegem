<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class EventController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne() {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['listTopic'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['listCategory'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['listPerson'] = \AnOrmApart\Dal::readAll('Person');
        $model['Message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            'Name' => $_POST['Name'],
            'Location' => $_POST['Location'],
            'Starts' => $_POST['Starts'],
            'Ends' => $_POST['Ends'],
            'Image' => $_POST['Image'],
            'Description' => $_POST['Description'],
            'PersonId' => $_POST['PersonId'],
            'OrganiserDescription' => $_POST['OrganiserDescription'],
            'EventCategoryId' => $_POST['EventCategoryId'],
            'EventTopicId' => $_POST['EventTopicId']
        );

        if (\AnOrmApart\Dal::create('Event', $event, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$event['Name']} is toegevoegd aan Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet toevoegen aan Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('Event', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('Event');
            return $this->view($model, 'Views/Event/Index.php');
        } else {
            return $this->view($model, 'Views/Event/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['listTopic'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['listCategory'] = \AnOrmApart\Dal::readAll('EventCategory');
        $model['listPerson'] = \AnOrmApart\Dal::readAll('Person');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/Event/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $event = array(
            "Id" => $_POST['Id'],
            'Name' => $_POST['Name'],
            'Location' => $_POST['Location'],
            'Starts' => $_POST['Starts'],
            'Ends' => $_POST['Ends'],
            'Image' => $_POST['Image'],
            'Description' => $_POST['Description'],
            'PersonId' => $_POST['PersonId'],
            'OrganiserDescription' => $_POST['OrganiserDescription'],
            'EventCategoryId' => $_POST['EventCategoryId'],
            'EventTopicId' => $_POST['EventTopicId']
        );

        $isUpdated = \AnOrmApart\Dal::update('Event', $event, 'Name');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('Event');
            return $this->view($model, 'Views/Event/Index.php');
        } else {
            return  $this->view($model, 'Views/Event/UpdatingOne.php');
        }
    }
}
