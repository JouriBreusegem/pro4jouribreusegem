<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class AdminController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model = array('title' => 'Admin Index');
        return $this->view($model);
    }
}
