<?php
/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class PersonController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Person');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne() {
        $model['list'] = \AnOrmApart\Dal::readAll('Person');
        $model['listCountry'] = \AnOrmApart\Dal::readAll('Country');
        $model['Message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function createOne()
    {
        $model = array(
            'tableName' => 'Person',
            'error' => 'Geen'
        );
        $person = array(
            'FirstName' => $_POST['FirstName'],
            'LastName' => $_POST['LastName'],
            'Email' => $_POST['Email'],
            'Address1' => $_POST['Address1'],
            'Address2' => $_POST['Address2'],
            'PostalCode' => $_POST['PostalCode'],
            'City' => $_POST['City'],
            'Phone1' => $_POST['Phone1'],
            'Birthday' => $_POST['Birthday'],
            'Rating' => $_POST['Rating'],
            'CountryId' => $_POST['CountryId']
        );

        if (\AnOrmApart\Dal::create('Person', $person, 'FirstName')) {
            $model['message'] = "Rij toegevoegd! {$person['FirstName']} is toegevoegd aan Person";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$person['FirstName']} niet toevoegen aan Person";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Person');
        return $this->view($model, 'Views/Person/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Person', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Person');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function deleteOne($Id)
    {
        $isDeleted = \AnOrmApart\Dal::delete('Person', $Id);
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isDeleted) {
            $model['list'] = \AnOrmApart\Dal::readAll('Person');
            return $this->view($model, 'Views/Person/Index.php');
        } else {
            return $this->view($model, 'Views/Person/ReadingOne.php');
        }
    }

    public function updatingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Person', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Person');
        $model['listCountry'] = \AnOrmApart\Dal::readAll('Country');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model, 'Views/Person/UpdatingOne.php');
    }

    public function updateOne($Id)
    {
        $model = array(
            'tableName' => 'Person',
            'error' => 'Geen'
        );
        $person = array(
            'Id' => $_POST['Id'],
            'FirstName' => $_POST['FirstName'],
            'LastName' => $_POST['LastName'],
            'Email' => $_POST['Email'],
            'Address1' => $_POST['Address1'],
            'Address2' => $_POST['Address2'],
            'PostalCode' => $_POST['PostalCode'],
            'City' => $_POST['City'],
            'Phone1' => $_POST['Phone1'],
            'Birthday' => $_POST['Birthday'],
            'Rating' => $_POST['Rating'],
            'CountryId' => $_POST['CountryId']
        );

        $isUpdated = \AnOrmApart\Dal::update('Person', $person, 'FirstName');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        if($isUpdated){
            $model['list'] = \AnOrmApart\Dal::readAll('Person');
            return $this->view($model, 'Views/Person/Index.php');
        } else {
            $model['list'] = \AnOrmApart\Dal::readAll('Person');
            return  $this->view($model, 'Views/Person/Index.php');
        }
    }
}