<?php
// var_dump($_SERVER);
use ThreepennyMVC\FrontController;
require 'vendor/autoload.php';

include ('Controllers/AdminController.php');
include ('Controllers/EventController.php');
include ('Controllers/EventCategoryController.php');
include ('Controllers/EventTopicController.php');
include ('Controllers/RoleController.php');
include ('Controllers/CountryController.php');
include ('Controllers/UserController.php');
include ('Controllers/PersonController.php');

$route = FrontController::getRouteData($_SERVER['REQUEST_URI'], 'Fricfrac', 'Admin', 'index');
$view = FrontController::dispatch($route);
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="/css/w3.css">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Fric-Frac Events</title>
</head>

<body>
    <header class="w3-row w3-padding w3-teal">
        <nav>
            <li class="w3-xxxlarge"><a href="/"><i class="fa fa-home"></i></a> Fric-Frac</li>
        </nav>
    </header>
    <?php echo $view(); ?>
    <footer>
        <p>Opdracht Programmeren 4</p>
    </footer>
</body>

</html>