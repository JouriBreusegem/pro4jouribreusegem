<div class="w3-row w3-container w3-red">
    <h1><?php echo $model['title']; ?></h1>
</div>

<div class="w3-row-padding w3-margin w3-center">
    <div class="w3-quarter">
        <a href="/EventCategory/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>Event Category</p>
            </div>
        </a>
    </div>
    <div class="w3-quarter">
        <a href="/EventTopic/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>Event Topic</p>
            </div>
        </a>
    </div>
    <div class="w3-quarter">
        <a href="/Event/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>Event</p>
            </div>
        </a>
    </div>
    <div class="w3-quarter">
        <a href="/Role/index">
            <div class="w3-card w3-light-green  w3-hover-shadow w3-padding-32 w3-center">
                <p>Role</p>
            </div>
        </a>
    </div>
</div>
<div class="w3-row-padding w3-margin w3-center">
    <div class="w3-quarter">
        <a href="/Country/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>Country</p>
            </div>
        </a>
    </div>
    <div class="w3-quarter">
        <a href="/User/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>User</p>
            </div>
        </a>
    </div>
    <div class="w3-quarter">
        <a href="/Person/index">
            <div class="w3-card w3-light-green w3-hover-shadow w3-padding-32 w3-center">
                <p>Person</p>
            </div>
        </a>
    </div>
    <div class="w3-container w3-col s12 m12 l12">Informatieve tegel</div>
</div>