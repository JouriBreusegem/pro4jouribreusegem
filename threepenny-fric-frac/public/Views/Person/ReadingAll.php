<aside>
    <div class="w3-col l4 m6 s12">
        <?php
        if ($model['list']) { ?>
            <table class="w3-bar-item w3-light-blue w3-hover-blue">
                <tr class="w3-orange">
                    <th></th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
                <?php
                foreach($model['list'] as $item) {
                    ?>
                    <tr>
                        <td>
                            <a
                                    href="/Person/readingOne/<?php echo $item['Id'];?>">
                                <span></span>
                                <span>Select</span></a>
                        </td>
                        <td><?php echo $item['FirstName'];?></td>
                        <td><?php echo $item['LastName'];?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        } else { ?>
            <p>Geen rijen gevonden in Person tabel.</p>
            <?php
        } ?>
    </div>
</aside>