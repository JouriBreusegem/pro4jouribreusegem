<section>
    <div class="w3-row w3-container w3-red">
        <h1>Event Topic</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/Person/UpdatingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Updating One</a>
                <a href="/Person/InsertingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Inserting One</a>
                <a href="/Person/deleteOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Delete One</a>
                <a href="/Person/Index/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <div class="w3-panel w3-gray">
                    <p>Selected Person</p>
                    <table class="w3-table-all w3-border">
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                        <tr>
                            <td><?php echo $model['row']['Id']; ?></td>
                            <td><?php echo $model['row']['FirstName']; ?></td>
                            <td><?php echo $model['row']['LastName']; ?></td>
                        </tr>
                    </table>
                    <p></p>
                </div>
                <div class="w3-card">
                    <form class="w3-container" id="form" method="post" action="/Country/createOne">
                        <p></p>
                        <label class="w3-text-blue" for="Name"><b>Naam</b></label>
                        <input class="w3-input w3-border" type="text" required id="Name" name="Name" value="<?php echo $model['row']['FirstName']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Code</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['LastName']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Email</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Email']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Address1</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Address1']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Address2</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Address2']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>PostalCode</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['PostalCode']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>City</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['City']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Phone1</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Phone1']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Birthday</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Birthday']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Rating</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['Rating']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Code"><b>Country ID</b></label>
                        <input class="w3-input w3-border" type="text" required id="Code" name="Code" value="<?php echo $model['row']['CountryId']; ?>" readonly>
                        <p></p>
                    </form>
                </div>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>