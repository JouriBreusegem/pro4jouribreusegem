<section>
    <div class="w3-row w3-container w3-red">
        <h1>Person</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/Person/Index/" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>

            <form class="w3-container"  id="form" method="post" action="/Person/updateOne">
                <p></p>
                <input class="w3-text-blue" type="hidden" id="Id" name="Id" value="<?php echo $model['row']['Id']; ?>" />
                <label class="w3-text-blue" for="FirstName"><b>First Name</b></label>
                <input class="w3-input w3-border"  type="text" required id="FirstName" name="FirstName" value="<?php echo $model['row']['FirstName']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="LastName"><b>Last Name</b></label>
                <input class="w3-input w3-border" type="text" required id="LastName" name="LastName" value="<?php echo $model['row']['LastName']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Email"><b>Email</b></label>
                <input class="w3-input w3-border" type="text" required id="Email" name="Email" value="<?php echo $model['row']['Email']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Address1"><b>Address1</b></label>
                <input class="w3-input w3-border" type="text" required id="Address1" name="Address1" value="<?php echo $model['row']['Address1']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Address2"><b>Address2</b></label>
                <input class="w3-input w3-border" type="text" required id="Address2" name="Address2" value="<?php echo $model['row']['Address2']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="PostalCode"><b>PostalCode</b></label>
                <input class="w3-input w3-border" type="text" required id="PostalCode" name="PostalCode" value="<?php echo $model['row']['PostalCode']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="City"><b>City</b></label>
                <input class="w3-input w3-border" type="text" required id="City" name="City" value="<?php echo $model['row']['City']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Phone1"><b>Phone1</b></label>
                <input class="w3-input w3-border" type="text" required id="Phone1" name="Phone1" value="<?php echo $model['row']['Phone1']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Birthday"><b>Birthday</b></label>
                <input class="w3-input w3-border" type="date" required id="Birthday" name="Birthday" value="<?php echo $model['row']['Birthday']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Rating"><b>Rating</b></label>
                <input class="w3-input w3-border" type="number" required id="Rating" name="Rating" value="<?php echo $model['row']['Rating']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="CountryId"><b>Country</b></label>
                <select class="w3-select w3-border" id="CountryId"  value="<?php echo $model['row']['Rating']; ?>" name="CountryId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listCountry']) {
                        foreach ($model['listCountry'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['Name'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <button type="submit" value="insert" class="w3-btn w3-blue">Update One</button>
            </form>
        </div>
    <?php include('ReadingAll.php'); ?>
</section>