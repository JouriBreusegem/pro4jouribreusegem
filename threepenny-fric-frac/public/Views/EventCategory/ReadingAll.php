<aside>
    <div class="w3-col l4 m6 s12">
        <?php
        if ($model['list']) { ?>
            <table class="w3-table w3-striped w3-bordered w3-hoverable">
                <tr class="w3-orange">
                    <th></th>
                    <th>Naam</th>
                </tr>
                <?php
                foreach($model['list'] as $item) {
                    ?>
                    <tr>
                        <td>
                            <a
                                    href="/EventCategory/readingOne/<?php echo $item['Id'];?>">
                                <span></span>
                                <span>Select</span></a>
                        </td>
                        <td><?php echo $item['Name'];?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        } else { ?>
            <p>Geen rijen gevonden in EventCategory tabel.</p>
            <?php
        } ?>
    </div>
</aside>