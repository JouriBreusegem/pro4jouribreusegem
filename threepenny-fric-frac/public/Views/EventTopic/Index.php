<section>
    <div class="w3-row w3-container w3-red">
        <h1>Eventtopic</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/eventTopic/InsertingOne" class="w3-bar-item w3-light-blue w3-hover-blue">InsertingOne</a>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
    <div>
        <div>
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </div>
</section>