<section>
    <div class="w3-row w3-container w3-red">
        <h1>Event Topic</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar w3-blue">
                <a href="/EventTopic/Index" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <form id="form" method="post" action="/EventTopic/createOne">
                    <p></p>
                    <label class="w3-text-blue" for="Name"><b>Naam</b></label>
                    <input class="w3-input w3-border" type="text" required id="Name" name="Name" />
                    <p></p>
                    <button class="w3-btn w3-blue" type="submit" value="insert" name="uc">Insert One</button>
                    <div>
                        <p><?php echo $model['message']; ?></p>
                        <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
                    </div>
                </form>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>