<section>
    <div class="w3-row w3-container w3-red">
        <h1>Country</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">

                <a href="/Country/Index/" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <form class="w3-container"  id="form" method="post" action="/Country/updateOne">
                <p>
                <input type="hidden" id="Id" name="Id" value="<?php echo $model['row']['Id']; ?>" />
                <label class="w3-text-blue" for="Name"><b>Naam</b></label>
                <input class="w3-input w3-border" id="Name" name="Name" type="text" value="<?php echo $model['row']['Name']; ?>" required />
                <p>
                <label class="w3-text-blue" for="Code"><b>Code</b></label>
                <input class="w3-input w3-border" id="Code" name="Code" type="text" value="<?php echo $model['row']['Code']; ?>" required />
                <p>
                <button class="w3-btn w3-blue">Update One</button>
            </form>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>