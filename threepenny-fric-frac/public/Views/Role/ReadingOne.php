<section>
    <div class="w3-row w3-container w3-red">
        <h1>Role</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/Role/UpdatingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Updating One</a>
                <a href="/Role/InsertingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Inserting One</a>
                <a href="/Role/deleteOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Delete One</a>
                <a href="/Role/Index/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <div class="w3-panel w3-gray">
                    <p>Selected <Role></Role></p>
                    <table class="w3-table-all w3-border">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                        </tr>
                        <tr>
                            <td><?php echo $model['row']['Id']; ?></td>
                            <td><?php echo $model['row']['Name']; ?></td>
                        </tr>
                    </table>
                    <p></p>
                </div>
                <div class="w3-card">
                    <form class="w3-container" id="form" method="post" action="/Country/createOne">
                        <p></p>
                        <label class="w3-text-blue" for="Name"><b>Naam</b></label>
                        <input class="w3-input w3-border" type="text" required id="Name" name="Name" value="<?php echo $model['row']['Name']; ?>" readonly/>
                        <p></p>
                    </form>
                </div>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>