<aside>
    <div class="w3-col l4 m6 s12">
        <?php
        if ($model['list']) { ?>
            <table class="w3-bar-item w3-light-blue w3-hover-blue">
                <tr class="w3-orange">
                    <th></th>
                    <th>Naam</th>
                    <th>PersonId</th>
                    <th>RoleId</th>
                </tr>
                <?php
                foreach($model['list'] as $item) {
                    ?>
                    <tr>
                        <td>
                            <a
                                    href="/User/readingOne/<?php echo $item['Id'];?>">
                                <span></span>
                                <span>Select</span></a>
                        </td>
                        <td><?php echo $item['Name'];?></td>
                        <td><?php echo $item['PersonId'];?></td>
                        <td><?php echo $item['RoleId'];?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        } else { ?>
            <p>Geen rijen gevonden in User tabel.</p>
            <?php
        } ?>
    </div>
</aside>