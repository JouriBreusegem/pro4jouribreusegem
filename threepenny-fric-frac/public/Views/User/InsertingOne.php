<section>
    <div class="w3-row w3-container w3-red">
        <h1>User</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar w3-blue">
                <a href="/User/Index" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <form id="form" method="post" action="/User/createOne">
                    <p></p>
                    <label class="w3-text-blue" for="Name"><b>Naam</b></label>
                    <input class="w3-input w3-border" type="text" required id="Name" name="Name" />
                    <p></p>
                    <label class="w3-text-blue" for="Salt"><b>Salt</b></label>
                    <input class="w3-input w3-border" type="text" required id="Salt" name="Salt" />
                    <p></p>
                    <label class="w3-text-blue" for="HashedPassword"><b>Hashed Password</b></label>
                    <input class="w3-input w3-border" type="text" required id="HashedPassword" name="HashedPassword" />
                    <p>
                    <label class="w3-text-blue" for="PersonId"><b>Person</b></label>
                    <select class="w3-select w3-border" id="PersonId" name="PersonId" readonly>
                        <?php
                        if ($model['listPerson']) {
                            foreach ($model['listPerson'] as $row) {
                                ?>
                                <option value="<?php echo $row['Id'];?>">
                                    <?php echo $row['FirstName'];?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <p></p>
                    <label class="w3-text-blue" for="RoleId"><b>Role</b></label>
                    <select class="w3-select w3-border" id="RoleId" name="RoleId" readonly>
                        <?php
                        if ($model['listRole']) {
                            foreach ($model['listRole'] as $row) {
                                ?>
                                <option value="<?php echo $row['Id'];?>">
                                    <?php echo $row['Name'];?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <p></p>
                    <button class="w3-btn w3-blue">Insert One</button>
                    <div>
                        <p><?php echo $model['Message']; ?></p>
                        <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
                    </div>
                </form>
            </div>

        </div>
        <?php include('ReadingAll.php'); ?>
    </div>

</section>