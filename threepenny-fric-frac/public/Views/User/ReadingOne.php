<section>
    <div class="w3-row w3-container w3-red">
        <h1>User</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/User/UpdatingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue"s>Updating One</a>
                <a href="/User/InsertingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Inserting One</a>
                <a href="/User/deleteOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Delete One</a>
                <a href="/User/Index/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <form id="form" method="" action="">
                    <div class="w3-panel w3-gray">
                        <p>Selected Person</p>
                        <table class="w3-table-all w3-border">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>PersonId</th>
                                <th>RoleId</th>
                            </tr>
                            <tr>
                                <td><?php echo $model['row']['Id']; ?></td>
                                <td><?php echo $model['row']['Name']; ?></td>
                                <td><?php echo $model['row']['PersonId']; ?></td>
                                <td><?php echo $model['row']['RoleId']; ?></td>
                            </tr>
                        </table>
                        <p></p>
                    </div>
                </form>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>