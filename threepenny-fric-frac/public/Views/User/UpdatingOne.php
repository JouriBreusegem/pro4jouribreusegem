<section>
    <div class="w3-row w3-container w3-red">
        <h1>Person</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/User/Index/" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>

            <form class="w3-container"  id="form" method="post" action="/User/updateOne">
                <input type="hidden" id="Id" name="Id" value="<?php echo $model['row']['Id']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Name"><b>Name</b></label>
                <input class="w3-input w3-border" type="text" required id="Name" name="Name" value="<?php echo $model['row']['Name']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Salt"><b>Salt</b></label>
                <input class="w3-input w3-border" type="text" required id="Salt" name="Salt" value="<?php echo $model['row']['Salt']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="HashedPassword"><b>Hashed Password</b></label>
                <input class="w3-input w3-border" type="text" required id="HashedPassword" name="HashedPassword" value="<?php echo $model['row']['HashedPassword']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="PersonId"><b>Person ID</b></label>
                <select class="w3-select w3-border" id="PersonId"  value="<?php echo $model['row']['PersonId']; ?>" name="PersonId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listPerson']) {
                        foreach ($model['listPerson'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['FirstName'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <label class="w3-text-blue" for="RoleId"><b>Role ID</b></label>
                <select class="w3-select w3-border" id="RoleId"  value="<?php echo $model['row']['RoleId']; ?>" name="RoleId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listRole']) {
                        foreach ($model['listRole'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['Name'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <button type="submit" value="insert" class="w3-btn w3-blue">Update One</button>
            </form>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>

</section>