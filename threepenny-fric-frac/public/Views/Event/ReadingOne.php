<section>
    <div class="w3-row w3-container w3-red">
        <h1>Eventcategory</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">
                <a href="/Event/UpdatingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Updating One</a>
                <a href="/Event/InsertingOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Inserting One</a>
                <a href="/Event/deleteOne/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Delete One</a>
                <a href="/Event/Index/<?php echo $model['row']['Id'];?>" class="w3-bar-item w3-light-blue w3-hover-blue">Annuleren</a>
            </div>
            <div class="w3-container">
                <div class="w3-panel w3-gray">
                    <p>Selected Category</p>
                    <table class="w3-table-all w3-boreder">
                        <tr>
                            <th>ID</th>
                            <th>Naam</th>
                            <th>Locatie</th>
                            <th>Organisator ID</th>
                        </tr>
                        <tr>
                            <td><?php echo $model['row']['Id']; ?></td>
                            <td><?php echo $model['row']['Name']; ?></td>
                            <td><?php echo $model['row']['Location']; ?></td>
                            <td><?php echo $model['row']['PersonId']; ?></td>
                        </tr>
                    </table>
                    <p></p>
                </div>
                <div class="w3-card">
                    <form class="w3-container" id="form" method="post" action="/Event/createOne">
                        <p></p>
                        <label class="w3-text-blue" for="Name">Naam</label>
                        <input class="w3-input w3-border" type="text" required id="Name" name="Name" value="<?php echo $model['row']['Name']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="Location">Location</label>
                        <input class="w3-input w3-border" type="text" required id="Location" name="Location" value="<?php echo $model['row']['Location']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="Starts">Start date</label>
                        <input class="w3-input w3-border" type="text" id="Starts" name="Starts" value="<?php echo $model['row']['Starts']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Ends">End date</label>
                        <input class="w3-input w3-border" type="text" id="Ends" name="Ends" value="<?php echo $model['row']['Ends']; ?>" readonly>
                        <p></p>
                        <label class="w3-text-blue" for="Image">Image</label>
                        <input class="w3-input w3-border" type="text" required id="Image" name="Image" value="<?php echo $model['row']['Image']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="Description">Description</label>
                        <input class="w3-input w3-border" type="text" required id="Description" name="Description" value="<?php echo $model['row']['Description']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="PersonId">Organiser Name</label>
                        <input class="w3-input w3-border" type="text" required id="PersonId" name="PersonId" value="<?php echo $model['row']['PersonId']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="OrganiserDescription">Organiser Description</label>
                        <input class="w3-input w3-border" type="text" required id="OrganiserDescription" name="OrganiserDescription" value="<?php echo $model['row']['OrganiserDescription']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="EventCategoryId">Event Category</label>
                        <input class="w3-input w3-border" type="text" required id="EventCategoryId" name="EventCategoryId" value="<?php echo $model['row']['EventCategoryId']; ?>" readonly/>
                        <p></p>
                        <label class="w3-text-blue" for="EventTopicId">Event Topic</label>
                        <input class="w3-input w3-border" type="text" required id="EventTopicId" name="EventTopicId" value="<?php echo $model['row']['EventTopicId']; ?>" readonly/>
                        <p></p>
                    </form>
                </div>
            </div>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>