<section>
    <div class="w3-row w3-container w3-red">
        <h1>Event</h1>
    </div>

    <div class="w3-row">
        <div class="w3-col l8 m6 s12">
            <div class="w3-bar  w3-blue">

                <a href="/Event/Index/" class="w3-bar-item w3-hover-light-blue">Annuleren</a>
            </div>
            <form class="w3-container"  id="form" method="post" action="/Event/updateOne">
                <input type="hidden" id="Id" name="Id" value="<?php echo $model['row']['Id']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Name">Naam</label>
                <input class="w3-input w3-border" type="text" required id="Name" name="Name" value="<?php echo $model['row']['Name']; ?>"/>
                <p></p>
                <label class="w3-text-blue" for="Location">Location</label>
                <input class="w3-input w3-border" type="text" required id="Location" name="Location" value="<?php echo $model['row']['Location']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Starts">Start date</label>
                <input class="w3-input w3-border" type="text" id="Starts" name="Starts" value="<?php echo $model['row']['Starts']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Ends">End date</label>
                <input class="w3-input w3-border" type="text" id="Ends" name="Ends" value="<?php echo $model['row']['Ends']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Image">Image</label>
                <input class="w3-input w3-border" type="text" required id="Image" name="Image" value="<?php echo $model['row']['Image']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="Description">Description</label>
                <input class="w3-input w3-border" type="text" required id="Description" name="Description" value="<?php echo $model['row']['Description']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="PersonId">Organiser Name</label>
                <select class="w3-select w3-border" id="PersonId" name="PersonId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listPerson']) {
                        foreach ($model['listPerson'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['FirstName'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <label class="w3-text-blue" for="OrganiserDescription">Organiser Description</label>
                <input class="w3-input w3-border" type="text" required id="OrganiserDescription" name="OrganiserDescription" value="<?php echo $model['row']['OrganiserDescription']; ?>" />
                <p></p>
                <label class="w3-text-blue" for="EventCategoryId">Event Category</label>
                <select class="w3-select w3-border" id="EventCategoryId" name="EventCategoryId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listCategory']) {
                        foreach ($model['listCategory'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['Name'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <label class="w3-text-blue" for="EventTopicId">Event Topic</label>
                <select class="w3-select w3-border" id="EventTopicId" name="EventTopicId" readonly>
                    <!-- option elementen -->
                    <?php
                    if ($model['listTopic']) {
                        foreach ($model['listTopic'] as $row) {
                            ?>
                            <option value="<?php echo $row['Id'];?>">
                                <?php echo $row['Name'];?>
                            </option>
                            <?php
                        }
                    }
                    ?>
                </select>
                <p></p>
                <button class="w3-btn w3-blue">Update One</button>
            </form>
        </div>
        <?php include('ReadingAll.php'); ?>
    </div>
</section>